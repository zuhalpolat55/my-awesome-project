﻿using IdentityModel.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using WindowsService.DTOs;
using WindowsService.Models;

namespace WindowsService
{
    public partial class Service1 : ServiceBase
    {
        private static readonly string LoggerPath = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["LoggerPath"]);
        private static readonly string BaseUrl = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ApiUrl"]);
        private static readonly int Interval = Convert.ToInt32(Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["Interval"]));
        private readonly System.Timers.Timer tmr = new System.Timers.Timer();
        private readonly HttpClient client = new HttpClient();
        private static List<Processes> processes = null;
        private static string ipAddress;
        private static string machineName;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var response = Authorization().Result;

            if (response) 
            {
                GetIpAddress();

                GetMachineName();

                Thread.Sleep(5000);

                processes = GetProcessList().Result;

                File.AppendAllText(LoggerPath, "Windows Service started at " + DateTime.Now.ToString() + "\r\n");
                tmr.Elapsed += new ElapsedEventHandler(Tmr_Elapsed);
                tmr.Interval = Interval;
                tmr.Start();
            }
        }

        private void Tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            GetProcessesData(processes);
            File.AppendAllText(LoggerPath, "\nWindows Service running at " + DateTime.Now.ToString() + "\r\n");
        }

        protected override void OnStop()
        {
            File.AppendAllText(LoggerPath, "Windows Service stopped at " + DateTime.Now.ToString() + "\r\n");
        }

        protected async Task<bool> Authorization()
        {
            var apiClient = new HttpClient();
            var IdentityUrl = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["IdentityUrl"]);

            var disco = await apiClient.GetDiscoveryDocumentAsync(IdentityUrl);

            if (disco.IsError)
            {
                File.AppendAllText(LoggerPath, "Error at requesting authorization. Details: " + disco.Error + " at " + DateTime.Now.ToString() + "\r\n");
                return false;
            }

            var tokenResponse = await apiClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ClientId"]),
                ClientSecret = Environment.ExpandEnvironmentVariables(ConfigurationManager.AppSettings["ClientSecret"])
            });

            if (tokenResponse.IsError)
            {
                File.AppendAllText(LoggerPath, "Error at requesting token for authorization. Details: " + tokenResponse.Error + " at " + DateTime.Now.ToString() + "\r\n");
                return false;
            }

            client.SetBearerToken(tokenResponse.AccessToken);

            File.AppendAllText(LoggerPath, "Login successfull at " + DateTime.Now.ToString() + "\r\n");

            return true;
        }

        private async Task<List<Processes>> GetProcessList()
        {
            List<Processes> processes = null;

            var response = await client.GetAsync(BaseUrl + "Process");

            if (!response.IsSuccessStatusCode)
            {
                File.AppendAllText(LoggerPath, "Error at requesting processes' name to be followed. Details: " + response.Content.ToString() + " at " + DateTime.Now.ToString() + "\r\n");
            }
            else
            {
                File.AppendAllText(LoggerPath, "The list of processes' name to be followed has been received successfully at " + DateTime.Now.ToString() + "\r\n");
                processes = await response.Content.ReadAsAsync<List<Processes>>();
            }

            return processes;
        }

        private void GetIpAddress()
        {
            IPAddress[] localps = Dns.GetHostAddresses(Dns.GetHostName());

            foreach (IPAddress addr in localps)
            {
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddress = addr.ToString();
                }
            }
            File.AppendAllText(LoggerPath, "Machine IP address: " + ipAddress + "\r\n");
        }

        private void GetMachineName()
        {
            machineName = Environment.MachineName;
            File.AppendAllText(LoggerPath, "Machine name: " + machineName + "\r\n");
        }

        private void GetProcessesData(List<Processes> processes)
        {
            var totalProcessData = GetTotalProcessData().Result;
            var response = PostProcessDataAsync(totalProcessData);

            foreach (Processes process in processes)
            {
                if (IsProcessRunning(process.Name))
                {
                    var processData = GetProcessDataByNameAsync(process.Name).Result;

                    response = PostProcessDataAsync(processData);
                }
            }
        }

        private async Task<string> PostProcessDataAsync(ProcessDataDTO processData)
        {
            var data = new StringContent(JsonConvert.SerializeObject(processData), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(BaseUrl + "ProcessData", data);

            if (!response.IsSuccessStatusCode)
            {
                File.AppendAllText(LoggerPath, processData.Name + "'s data could not send. The Error: " + response.Content.ReadAsStringAsync() + " at " + DateTime.Now.ToString() + "\r\n");
            }

            return response.Content.ReadAsStringAsync().Result;
        }

        private static async Task<ProcessDataDTO> GetProcessDataByNameAsync(String processName)
        {
            var cpuCounter = new PerformanceCounter("Process", "% Processor Time", processName);
            var ramCounter = new PerformanceCounter("Process", "Working Set - Private", processName);
            var diskCounter = new PerformanceCounter("Process", "IO Data Operations/sec", processName);

            cpuCounter.NextValue();
            ramCounter.NextValue();
            diskCounter.NextValue();
            await Task.Delay(1000);

            return new ProcessDataDTO(ipAddress, machineName, processName, cpuCounter.NextValue(), ramCounter.NextValue() / 1024 / 1024, diskCounter.NextValue() / 1024);
        }

        private static async Task<ProcessDataDTO> GetTotalProcessData()
        {
            var cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            var ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            var diskCounter = new PerformanceCounter("PhysicalDisk", "% Disk Time", "_Total");

            cpuCounter.NextValue();
            ramCounter.NextValue();
            diskCounter.NextValue();
            await Task.Delay(1000);

            return new ProcessDataDTO(ipAddress, machineName, "_Total", cpuCounter.NextValue(), ramCounter.NextValue(), diskCounter.NextValue() / 1024);
        }

        private static bool IsProcessRunning(string processName)
        {
            return Process.GetProcessesByName(processName).Length > 0;
        }
    }
}
