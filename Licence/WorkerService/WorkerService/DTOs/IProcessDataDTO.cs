using System;

namespace WorkerService.DTOs
{
    public interface IProcessDataDTO
    {
        string IpAddress { get; set; }
        string MachineName { get; set; }
        string Name { get; set; }
        float Cpu { get; set; }
        float Ram { get; set; }
        float Disk { get; set; }
        DateTime PTimestamp { get; set; }
    }
}