using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WorkerService.DTOs;
using WorkerService.Models;

namespace WorkerService
{
    public class Worker : BackgroundService
    {
        private readonly static string BaseUrl = "http://localhost:5001/api/";
        private HttpClient client = new HttpClient();
        private readonly ILogger<Worker> _logger;
        private static string ipAddress;
        private static string machineName;

            public Worker(ILogger<Worker> logger)
            {
                _logger = logger;
            }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Worker service started at: {time}", DateTime.Now);

            Authorization();

            GetIpAddress();

            GetMachineName();

            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Delay(3000);

            List<Processes> processes = GetProcessList().Result;

            while (!stoppingToken.IsCancellationRequested)
            {
                GetProcessesData(processes);

                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(9000, stoppingToken);
            }
        }

        protected async void Authorization()
        {
            var apiClient = new HttpClient();

            var disco = await apiClient.GetDiscoveryDocumentAsync("http://localhost:6001");

            if(disco.IsError)
            {
                _logger.LogError(disco.Error);
                return;
            }

            var tokenResponse = await apiClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "LicenceService",
                ClientSecret = "secret"
            });

            if(tokenResponse.IsError)
            {
                _logger.LogError(tokenResponse.Error);
                return;
            }

            client.SetBearerToken(tokenResponse.AccessToken);
        }

        private async Task<List<Processes>> GetProcessList()
        {
            List<Processes> processes = null;

            var response = await client.GetAsync(BaseUrl + "Process");

            if(!response.IsSuccessStatusCode)
            {
                _logger.LogError(response.StatusCode.ToString());
            }
            else
            {
                processes = await response.Content.ReadAsAsync<List<Processes>>();
            }

            return processes;
        }

        private void GetIpAddress()
        {
            IPAddress[] localps = Dns.GetHostAddresses(Dns.GetHostName());

            foreach(IPAddress addr in localps)
            {
                if(addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    ipAddress = addr.ToString();
                }
            }
            _logger.LogInformation("Machine IP address: {ipAddress}", ipAddress);
        }

        private void GetMachineName()
        {
            machineName = Environment.MachineName;
            _logger.LogInformation("Machine name: {machineName}", machineName);
        }

        private void GetProcessesData(List<Processes> processes)
        {
            var totalProcessData = GetTotalProcessData().Result;
            var response  = PostProcessDataAsync(totalProcessData);

            foreach (Processes process in processes)
            {
                if (IsProcessRunning(process.Name))
                {
                    var processData = GetProcessDataByNameAsync(process.Name).Result;

                    response = PostProcessDataAsync(processData);
                }
            }
        }

        private async Task<string> PostProcessDataAsync(ProcessDataDTO processData)
        {
            var data = new StringContent(JsonConvert.SerializeObject(processData), Encoding.UTF8, "application/json");
            var response = await client.PostAsync(BaseUrl + "ProcessData", data);

            return response.Content.ReadAsStringAsync().Result;
        }

        private static async Task<ProcessDataDTO> GetProcessDataByNameAsync(String processName)
        {
            var cpuCounter = new PerformanceCounter("Process", "% Processor Time", processName);
            var ramCounter = new PerformanceCounter("Process", "Working Set - Private", processName);
            var diskCounter = new PerformanceCounter("Process", "IO Data Operations/sec", processName);

            cpuCounter.NextValue();
            ramCounter.NextValue();
            diskCounter.NextValue();
            await Task.Delay(1000);

            return new ProcessDataDTO(ipAddress, machineName, processName, cpuCounter.NextValue(), ramCounter.NextValue() / 1024 / 1024, diskCounter.NextValue() / 1024);
        }

        private static async Task<ProcessDataDTO> GetTotalProcessData()
        {
            var cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            var ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            var diskCounter = new PerformanceCounter("PhysicalDisk", "% Disk Time", "_Total");

            cpuCounter.NextValue();
            ramCounter.NextValue();
            diskCounter.NextValue();
            await Task.Delay(1000);

            return new ProcessDataDTO(ipAddress, machineName, "_Total", cpuCounter.NextValue(), ramCounter.NextValue(), diskCounter.NextValue() / 1024);
        }

        private static bool IsProcessRunning(string processName)
        {
            return Process.GetProcessesByName(processName).Length > 0;
        }
    }
}

