using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MassTransit;
using System;
using GreenPipes;
using API.Consumers;
using API.Models;

namespace API.Extensions
{
    public static class RabbitMQServiceExtensions
    {
        public static IServiceCollection AddRabbitMQServices(this IServiceCollection services, IConfiguration config)
        {
            var rabbitMqConsts = config.GetSection(RabbitMqConsts.SectionName).Get<RabbitMqConsts>();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<ProcessDataConsumer>();
                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.UseHealthCheck(provider);
                    cfg.Host(new Uri(rabbitMqConsts.RabbitMqUri),h =>
                    {
                        h.Username(rabbitMqConsts.Username);
                        h.Password(rabbitMqConsts.Password);
                    });
                    cfg.ReceiveEndpoint(rabbitMqConsts.ProcessDomainService, ep =>
                    {                       
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(r => r.Interval(2, 100));
                        ep.ConfigureConsumer<ProcessDataConsumer>(provider);
                    });
                }));
            });
            services.AddMassTransitHostedService();

            return services;
        }
    }
}