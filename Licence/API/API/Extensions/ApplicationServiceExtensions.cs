using API.Data;
using API.Helpers;
using API.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using API.Models;

namespace API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddScoped<IProcessRepository, ProcessRepository>();
            services.AddScoped<IProcessDataRepository, ProcessDataRepository>();
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlite(config.GetConnectionString("DefaultConnection"));
            });
            services.AddAutoMapper(typeof(AutoMapperProfile).Assembly);
            services.AddOptions(); // IOptions<T>

            services.Configure<RabbitMqConsts>(config.GetSection(RabbitMqConsts.SectionName));
            services.Configure<IdentityService>(config.GetSection(IdentityService.SectionName));

            return services;
        }
    }
}