using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using API.Models;

namespace API.Extensions
{
    public static class IdentityServiceExtensions
    {
        public static IServiceCollection AddIdentityServices(this IServiceCollection services, IConfiguration config)
        {
            var identityService = config.GetSection(IdentityService.SectionName).Get<IdentityService>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.Authority = identityService.JwtBearerOptions.Authority;
                    options.Audience = identityService.JwtBearerOptions.Audience;
                    options.RequireHttpsMetadata = identityService.JwtBearerOptions.RequireHttpsMetadata;
                });

            return services;
        }
    }
}