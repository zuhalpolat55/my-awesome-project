namespace API.Models
{
    public class RabbitMqConsts
    {
        public const string SectionName = "RabbitMqConsts";
        public string RabbitMqUri { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ProcessDomainService { get; set; }
    }
}