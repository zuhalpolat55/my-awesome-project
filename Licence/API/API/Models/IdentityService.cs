namespace API.Models
{
    public class IdentityService
    {
        public const string SectionName = "IdentityService";

        public JwtBearer JwtBearerOptions { get; set; }

    }
    public class JwtBearer
    {
        public string Authority { get; set; }
        public string Audience { get; set; }
        public bool RequireHttpsMetadata { get; set; }
    }
}