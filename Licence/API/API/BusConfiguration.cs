using API.Models;
using MassTransit;
using MassTransit.RabbitMqTransport;
using Microsoft.Extensions.Options;
using System;

namespace API
{
    public static class BusConfigurator
    {
        public static IBusControl ConfigureBus(Action<IRabbitMqBusFactoryConfigurator> registrationAction = null, IOptions<RabbitMqConsts> _options = null)
        {
            return Bus.Factory.CreateUsingRabbitMq(cfg =>
             {
                 cfg.Host(new Uri(_options.Value.RabbitMqUri), hst =>
                 {
                    hst.Username(_options.Value.Username);
                    hst.Password(_options.Value.Password);
                 });
               
                 registrationAction?.Invoke(cfg);
             });
        }
    }

}