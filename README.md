Merhaba ben Zuhal Polat,

Burada stajım sırasında yapmış olduğum license source management projesinin sunumunu yapacağım.

İlk olarak license source management projesi nedir be biz bu proje ile neyi hedeflemekteyiz bundan kısaca bahsedeyim.

Bu proje şirket içerisinde takip etmek istediğimiz matlab ve benzeri lisanslı uygulamaları kullanan kullanıcıların bu uygulamaları veya uygulamayı kullandıkları sırada uygulamanın bilgisayarda harcadığı cpu, ram ve disk verilerini belirli aralıklarla topladığımız bir projedir.

Amacımız ise uzun vadede aldığımız bu verilerin analizini yaparak kullanıcıların lisansını aldıkları bu uygulamaları bilgisayarlarında aktif bir şekilde kullanıp kullanmadığının tespitini yapmayı hedeflemekteyiz.

Kullanıcıların bilgisayarlarından CPU, RAM ve Disk verilerini arka planda çalışacak bir windows servis ile almaktayız. Bu windows servis  bilgisayarda arka planda çalışarak her 10 saniyede bir uygulamanın verilerini sunucuya gönderecek.

Bilgisayarlardan bu verileri Windows'un yazılım ürünlerinden biri olan perfmon.exe ile almaktayız. Perfmon yani performans izleyici, bilgisayar üzerindeki işlemlerin takip edilerek, eş zamanlı grafiksel verilerin çıkarılmasını sağlayan yönetimsel bir araç olarak kullanılmakta. Fotoğrafta görüldüğü gibi burada Process kategorisi altında chrome uygulamasının processor time yani harcadığı cpu verisini, user time ve benzeri verileri eş zamanlı grafiksel olarak alabilmekteyiz. 

Bizlerde bu projede takibini yapmak istediğimiz uygulamaların cpu ram ve disk verilerini eş zamanlı olarak windows servis aracılığıyla bu uygulamadan alıyoruz. Perfmon'dan verileri almak için .net'in kendi kütüphanelerinden biri olan Performance Counter kütüphanesini kullanmaktayız. 

Projede windows service, ASP.NET CORE, Angular JS, RabbitMq ve sql server kullanmaktayız. Projede Angular ile oluşturduğumuz web arayüzü ve windows service olarak clientlarımız bulunmakta. windows service biraz önce bahsettiğim gibi bilgisayarlardan verileri toplamakta. Web arayüzünde ise alınan bu verileri grafiksel olarak göstermekte ve takibi yapılacak uygualamlarla ilgili düzenleme yapılabilmekte. Bu clientlar web servislerinden rest api kullanarak API ile haberleşebilmekte. API asp.net ile yazıldı. verilerin kaydedilmesi için de sql server kullanılmakta.

Diğer bir kullandığımız teknoloji ise RabbitMq. Bu projede her bir kullanıcıdan bir veya birden fazla uygulama için her 10 saniyede veri almaktayız. Sistemde oluşabilecek yoğunluğa karşı ölçeklenbilirliği sağlamak adına RabbitMq kullanmaktayız. Gelen veriler .net'de yazdığımız publisher ile veriyi rabbitmq de oluşturduğumuz kuyruğa yazmakta ve bu verilerde sırası geldiğinde consumer ile alınarak veri tabanına kaydedilmektedir.

Web arayüzü ile API arasındaki authentication için ise web uygulamaları ve RESTful web hizmetleri için tek oturum açma çözümü olan ve şirket içerisinde de authorization için kullanılan KeyCloak kullanılmıştır.

Windows Service ile API arasında ise bir kullanıcının manuel olarak bir arayüz aracılığıyla authenticat edilmesi beklenmemekte. Burada Kullanıcıdan ziyade client'ın yani windows servisin authenticate edilmesi lazım. Yani Windows servis başladığı zaman uygulamanın kullanıcıdan bağımsız olarak authenticate edilerek istek atabilmesi gerekmekte. Bu şekilde Client’ın doğrulanması için machine-to-machine kimliklendirme denilen yöntemi kullanmaktayız. Machine-to-machine kimliklendirme için “Client Credentials Grant type” kullanıyoruz.

-------------



